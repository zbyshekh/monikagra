//POP_2015_12_14_projekt_1_CODE::BLOCKS 13.12_GNU CPP COMPILER
//do poprawnego dzialania potrzebny jest plik testowe mapki, ktory rowniez przyslalam

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

#define ROZMIAR_X 15
#define ROZMIAR_Y 14

#define MOZNA_JESC -1
#define WOLNE 1

using namespace std;

struct gracz
{
	int x;
	int y;
};

int kierunki[8][3] =
{ //Y  X
	2, -2, 1,			// 1
	2, 0, 2,			// 2
	2, 2, 3,			// 3
	0, -2, 4,  			// 4
	-2, +2, 9,			// 9
	-2, 0, 8, 			// 8
	-2, -2, 7,			// 7
	0, 2, 6				// 6
};


void wyswietl_zasady()
{
	cout << " Gra w malpke i gwiazdke przeznaczona jest dla 2 graczy.\n Gracze poruszaja sie na zmiane.\n Ruch polega na przemieszczeniu wybranej figury o jedno \n pole zgodnie z krawedziami narysowanymi na planszy.\n Malpka ma mozliwosc zjadania gwiazdek.\n Zjadanie odbywa sie podobnie jak w warcabach- malpka przeskakuje na wolne\n pole zaraz za gwiazdka, zgodnie z krawedziami planszy. \n Jesli malpka ma mozliwosc zjedzenia gwiazki to musi wykonac taki ruch.\n W jednym ruchu mozna wielokrotnie zjadac gwiazdki, ale zjadanie nie moze byc \n wymuszone.\n Malpka wygrywa jesli zje wszystkie gwiazdki lub dojdzie do pola A3, A4 lub A5. \n Gwiazdki wygrywaja gdy ruch malpki zostanie zablokowany." << endl;
	cin.get();
	cin.get();
}

void panel_pomocy(char mapa[ROZMIAR_Y][ROZMIAR_X])
{
	string komenda = "z";

	while (komenda == "z")
	{

		cout << "\t\t\tPANEL POMOCY" << endl;
		cout << "1. Podaj z - zeby wyswietlic zasady gry" << endl;
		cout << "2. Zeby zobaczyc historie, zajrzyj do pliku: zapis_gry.txt" << endl;
		cout << "3. Podaj jakikolwiek inny klawisz, zeby rozpoczac" << endl;
		cin >> komenda;
		if (komenda == "z" || komenda == "Z")
			wyswietl_zasady();
		else		{
			cin.get();
			break;
		}
		system ("cls");
	}

}

void zapisz_do_pliku_historii(char mapa[ROZMIAR_Y][ROZMIAR_X], bool powitanie = false)
{
	ofstream plik;
	plik.open("zapis_gry.txt", ios::app);
	if (plik.is_open())
	{
		if (powitanie) plik << "POCZATEK ROZGRYWKI" << endl;

		for (int y = 0; y < ROZMIAR_Y; y++)
		{
			for (int x = 0; x < ROZMIAR_X; x++)
			{
				plik << mapa[y][x];
			}
			plik << endl;
		}
		plik << endl;
		plik.close();
	}
	else
	{
		cout << "Blad otwarcia pliku do zapisu" << endl;
	}
}

void wczytaj_mape(char mapa[ROZMIAR_Y][ROZMIAR_X], ifstream &plik, int &liczba_gwiazdek, gracz &malpka)
{
	liczba_gwiazdek = 0;
	malpka.x=-1;
	malpka.y=-1;

	plik.open("POP_2015_12_14_projekt_1_testowe_mapki1.txt");
	string linia;
	if (plik.is_open())
	{
		for (int y = 0; y < ROZMIAR_Y; y++)
		{
			getline(plik, linia);
			//linia[ROZMIAR_Y + 1] = '\0';

			for (int x = 0; x < ROZMIAR_X; x++)
			{
				mapa[y][x] = linia[x];
				if (linia[x] == '*') liczba_gwiazdek++;
				 if (linia[x] == '@'){
                    if(malpka.x==-1){
                        malpka.x = x;
                        malpka.y = y;
                    }else{
                        cout << "ej! masz za dużo małp!";
                    }
				}
			}
		}
	}
	else
	{
		cout << "Blad otwarcia pliku" << endl;
		cin.get();
		return;
	}

	cout << "Wczytano pomyslnie mape" << endl;
	plik.close();
}

void wyswietl_mape(char mapa[ROZMIAR_Y][ROZMIAR_X]) {
	for (int y = 0; y < ROZMIAR_Y; y++){
		for (int x = 0; x < ROZMIAR_X; x++){
			cout << mapa[y][x];
		}
		cout << endl;
	}
	cout << endl;
}

bool wykracza_poza_mape(int y, int x)
{
	if (x <= 5 && y <= 3) return true;
	if (x >= 11 && y <= 3) return true;
	if (x <= 5 && y >= 9) return true;
	if (x >= 11 && y >= 9) return true;

	if (y >= ROZMIAR_Y || y < 0 || x <= 1 || x >= ROZMIAR_X)
		return true;

	return false;
}

bool po_krawedziach(char kierunek, char mapa[ROZMIAR_Y][ROZMIAR_X], int y, int  x)
{ //PIONEK CHODZI PO SLASHASH

	switch (kierunek)
	{
	case '1':
		if (mapa[y + 1][x - 1] != '/') return false;
		break;

	case'9':
		if (mapa[y - 1][x + 1] != '/')return false;
		break;

	case'3':
		if (mapa[y + 1][x + 1] != '\\') return false;
		break;
	case '7':
		if (mapa[y - 1][x - 1] != '\\') return false;
		break;

	default: return true;
	}

	return true;
}

int prawdilowo_podany_kierunek(char kierunek, int &nowe_y, int &nowy_x, int &gracz_y, int &gracz_x)
{
	nowy_x = gracz_x;
	nowe_y = gracz_y;
	switch (kierunek)
	{
		// SW
	case '1': nowe_y += 2; nowy_x += -2; break;
		// S
	case '2': nowe_y += 2; break;
		// SE
	case '3': nowe_y += 2; nowy_x += 2; break;
		// w
	case '4': nowy_x += -2; break;
		// NE
	case '9': nowe_y += -2; nowy_x += 2; break;
		// N
	case '8': nowe_y += -2; break;
		// NW
	case '7': nowe_y += -2; nowy_x += -2; break;
		// W
	case '6': nowy_x += 2; break;
	default:
      return 0;
	}
	return 1;
}


int czy_moze_bic(gracz &malpka, char mapa[ROZMIAR_Y][ROZMIAR_X], bool &musi_bic, int &ile_gwiazdek){
	// we wszystkich mo¿liwych ruchach,
	// jesli jest gwiazdka
	int x, y;
	//x = malpka.x; y = malpka.y;

	// jeœli jest mo¿liwe bicie w jakimkolwiek kierunku
	 for (int i = 0; i < 8; i++)	{
		y = malpka.y; y += kierunki[i][0];
		x = malpka.x; x += kierunki[i][1];


		if (mapa[y][x] == '*' && !wykracza_poza_mape(y, x) && po_krawedziach(kierunki[i][2] + '0' , mapa, y, x))
		{
			int gwiazdka_y = y; y += kierunki[i][0];
			int gwiazdka_x = x; x += kierunki[i][1];

			if (mapa[y][x] == ' ' && !wykracza_poza_mape(y, x) && po_krawedziach(kierunki[i][2] + '0', mapa, y, x))
			{
				if (musi_bic)
				{
					cout << "Gracz Malpka musi zbic gwiazdke na polu " << char('G' - gwiazdka_y /2) << gwiazdka_x / 2 << endl;
					cout << "Potwierdz" << endl;
					cin.get();
					cin.get();
					cin.get();

					ile_gwiazdek--;
					// wyczysc pole malpki
					mapa[malpka.y][malpka.x] = ' ';
					malpka.y = y;
					malpka.x = x;
					mapa[malpka.y][malpka.x] = '@';
					mapa[gwiazdka_y][gwiazdka_x] = ' ';
					musi_bic = false;
					zapisz_do_pliku_historii(mapa);
				}
				return 1;
			}
		}
	}
	return 0;
}


int czy_wolne_pole (gracz &malpka, char kierunek_malpki, char mapa[ROZMIAR_Y][ROZMIAR_X],
                int &nowe_y, int &nowy_x, int &gwiazdka_y, int &gwiazdka_x, bool to_malpka = true){
	if (prawdilowo_podany_kierunek(kierunek_malpki, nowe_y, nowy_x, malpka.y, malpka.x) == 0){
		if(to_malpka) cout << "Podano zly kierunek. Podaj ponownie ruch" << endl;
		return 0;
	}

	if (wykracza_poza_mape(nowe_y, nowy_x)){
		if (to_malpka) cout << "Punkt polozony za mapa. Podaj ponownie ruch" << endl;
		return 0;
	}

	if (!po_krawedziach(kierunek_malpki, mapa, malpka.y, malpka.x)){
		if (to_malpka) cout << "Musisz wykonac ruch po krawedziach. Podaj ponownie ruch: " << endl;
		return 0;
	}


	if (mapa[nowe_y][nowy_x] == '*')
	{
		// jeœli za gwiazdk¹ w tym kierunku, stoi inna gwiazdka, to jest to b³êdne posuniêcie
		gwiazdka_y = nowe_y;
		gwiazdka_x = nowy_x;

		prawdilowo_podany_kierunek(kierunek_malpki, nowe_y, nowy_x, gwiazdka_y, gwiazdka_x);
		// jest w obrebie mapy
		if (!wykracza_poza_mape(nowe_y, nowy_x))
		{
			// i zaœ jeœli to pole jest wolne, mo¿na biæ, zwróc -1
			if (mapa[nowe_y][nowy_x] == ' ')
			{
				return MOZNA_JESC;
			}
		}

		if (to_malpka) cout << "Nie mozesz zajac pozycji przeciwnika. Podaj ponownie ruch: " << endl;
		return 0;
	}
	return 1;
}

bool malpka_wygrala( gracz  &malpka, int &ile_gwiazdek, int &koniec_gry)
{
	if (ile_gwiazdek == 0 || malpka.y == 12)
	{
		koniec_gry = 1;
		return 1;
	}
	return false;
}


bool ruch_malpki(char mapa[ROZMIAR_Y][ROZMIAR_X], int &koniec_gry, gracz &malpka, int &ile_gwiazdek)
{
	string kierunek_malpki;
	int nowy_x = malpka.x, nowe_y = malpka.y;
	bool musi_bic = true;
	int  gwiazdka_y = 0;
	int  gwiazdka_x = 0;

	cout << "GRACZ MALPKA:  ";

	while (czy_moze_bic(malpka, mapa, musi_bic,  ile_gwiazdek))
	{
		if (malpka_wygrala(malpka, ile_gwiazdek, koniec_gry)) return 1;

		wyswietl_mape(mapa);
		cout << "GRACZ MALPKA PONOWNIE: ";

		// gracz podaje  nowy kierunek
		cin >> kierunek_malpki;

		if (kierunek_malpki == "pomoc")
		{
			panel_pomocy(mapa);
			cout << "GRACZ MALPKA: ";
			cin >> kierunek_malpki;
		}

		int pole = czy_wolne_pole(malpka, kierunek_malpki[0], mapa, nowe_y, nowy_x, gwiazdka_y, gwiazdka_x);

		// jesli pole w kierunku podanym przez gracza jest wolne, czyli gracz zaniecha³ bicia i podal poprawne dane,
		// umieœæ tam ma³pkê, zakoñcz ruch,
		if (pole == WOLNE)
		{
			mapa[malpka.y][malpka.x] = ' ';
			malpka.y = nowe_y;
			malpka.x = nowy_x;
			mapa[malpka.y][malpka.x] = '@';
			wyswietl_mape(mapa);
			zapisz_do_pliku_historii(mapa);
			return 1;
		}
		// je¿eli jednak gracz zdecydowa³ siê na zjedzenie w podanym przez siebie kierunku
		// (bo mo¿e mieæ wiêcej mo¿liwoœci bicia, zjedz gwiazdke)
		else if (pole == MOZNA_JESC)
		{
			mapa[malpka.y][malpka.x] = ' ';
			malpka.y = nowe_y;
			malpka.x = nowy_x;
			mapa[malpka.y][malpka.x] = '@';
			mapa[gwiazdka_y][gwiazdka_x] = ' ';
			ile_gwiazdek--;
			zapisz_do_pliku_historii(mapa);
			wyswietl_mape(mapa);
			cout << "GRACZ MALPKA PONOWNIE: ";
			if (malpka_wygrala( malpka, ile_gwiazdek, koniec_gry)) return 1;
		}
		else
		{
			// dopóki wprowadza zle dane
			musi_bic = false;
		}
	}

	// jesli malpka nie musi bic, pobierz kierunek i wykonaj ruch
	cin >> kierunek_malpki;

	if (czy_wolne_pole(malpka, kierunek_malpki[0], mapa, nowe_y, nowy_x, gwiazdka_y, gwiazdka_x) == 1)
	{
		// jesli mo¿e, nanieœ na pozycjê
		mapa[malpka.y][malpka.x] = ' ';
		malpka.y = nowe_y;
		malpka.x = nowy_x;
		mapa[malpka.y][malpka.x] = '@';
		wyswietl_mape(mapa);
	}
	else
	{
		return 0;
	}

	return 1;
}

// Funkcja sprawdza, czy malpkaw jakimkolwiek kierunku moze siê poruszyæ
bool gwiazdki_wygraly(char mapa[ROZMIAR_Y][ROZMIAR_X], gracz  &malpka, int &ile_gwiazdek, int &koniec_gry)
{
	int nowe_y = 0;
	int nowy_x = 0;
	int gwiazdka_y = 0; int gwiazdka_x = 0;

	// jeœli w zadnym kierunki nie ma mozliwosci ruchu i bicia, gwiazdki zablokowa³y ma³pkê
	for (int i = 0; i < 8; i++)
	{
		if (czy_wolne_pole(malpka, kierunki[i][2] + '0', mapa, nowe_y, nowy_x, gwiazdka_y, gwiazdka_x, false) != 0)
		{
			return false;
		}
	}
	return true;
}
// w przypadku poprawnego posuniêcia zwróæ 1;
// przeciwnie 0;
int ruch_gwiazdek(char mapa[ROZMIAR_Y][ROZMIAR_X], int &koniec_gry)
{
    gracz gwiazdka;
	string kierunek_gwiazdek;
	int nowe_y, nowy_x;

	cout << "GRACZ GWIAZDKI: ";
	cin >> kierunek_gwiazdek;
	if (kierunek_gwiazdek == "pomoc")
	{
		panel_pomocy(mapa);
		//cout << "GRACZ GWIAZDKI: ";
		//cin >> kierunek_gwiazdek;
	}

	int y, x;

	y = (kierunek_gwiazdek[0] - 'G') * (-2);
	if (y < 0 || y >= ROZMIAR_Y)
	{
		cout << "Blednie okreslona gwiazdka. Podaj ponownie ruch" << endl;
		return 0;
	}
	x = (kierunek_gwiazdek[1] - '0') * 2;
	if (x < 0 || x >= ROZMIAR_X)
	{
		cout << "Blednie okreslona gwiazdka. Podaj ponownie ruch" << endl;
		return 0;
	}

	if (mapa[y][x] != '*')
	{
		cout << "Nie ma takiej gwiazdki. Podaj ponownie ruch" << endl;
		return 0;
	}
	gwiazdka.y = y; gwiazdka.x = x;

	if (prawdilowo_podany_kierunek(kierunek_gwiazdek[2], nowe_y, nowy_x, gwiazdka.y, gwiazdka.x) == 0)
	{
		cout << "Podano zly kierunek. Podaj ponownie ruch " << endl;
		return 0;
	}

	if (wykracza_poza_mape(nowe_y, nowy_x))
	{
		cout << "Punkt polozony za mapa. Podaj ponownie ruch" << endl;
		return 0;
	}

	if (!po_krawedziach(kierunek_gwiazdek[2], mapa, gwiazdka.y, gwiazdka.x))
	{
		cout << "Musisz wykonac ruch po krawedziach. Podaj ponownie ruch: " << endl;
		return 0;
	}

	// zosta³o przydzielone nowy_x i nowe_y, inaczej by tu nie dosz³o
	if (mapa[nowe_y][nowy_x] != ' ') {
		cout << "Pole zajete. Podaj ponownie ruch: " << endl;
		return 0;
	}

	// jesli kierunek jest poprawny, nanieœ na now¹ pozycjê GWIAZDKE
	mapa[gwiazdka.y][gwiazdka.x] = ' ';
	gwiazdka.y = nowe_y;
	gwiazdka.x = nowy_x;
	mapa[gwiazdka.y][gwiazdka.x] = '*';

	wyswietl_mape(mapa);
	zapisz_do_pliku_historii(mapa);
	return 1;
}



int main()
{
	char mapa[ROZMIAR_Y][ROZMIAR_X];
	ifstream plik;
	int koniec_gry = 0;
	gracz malpka;
//	gracz gwiazdki;
	int ile_gwiazdek = 0;
	remove("zapis_gry.txt");
	wczytaj_mape(mapa, plik, ile_gwiazdek, malpka);

	string kierunek_malpki;
	string kierunek_gwiazdek;

	panel_pomocy(mapa);

	wyswietl_mape(mapa);
	zapisz_do_pliku_historii(mapa, true);
//    cout << "gwiazdki" << gwiazdki.x;
	while (koniec_gry == 0){
		while (ruch_gwiazdek(mapa, koniec_gry) == 0);
		if (gwiazdki_wygraly(mapa, malpka, ile_gwiazdek, koniec_gry)) break;

		while (ruch_malpki(mapa, koniec_gry, malpka, ile_gwiazdek) == 0);
		if (malpka_wygrala(malpka, ile_gwiazdek, koniec_gry)) break;			// po poprawnym ruchu malpki sprawdcz, czy nie wygrala

		zapisz_do_pliku_historii(mapa);
	}

	wyswietl_mape(mapa);
	if (koniec_gry == 1)
	{
		cout << endl << endl << "Wygrala malpka" << endl;

	}
	else
	{
		cout << "Wygraly gwiazdki" << endl;
	}
	cin.get();
	cin.get();
	return 0;
}